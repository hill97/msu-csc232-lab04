/**
 * @file   Node.h
 * @author Jim Daehn
 * @brief  TODO: Give brief description of this class.
 */

#ifndef NODE_H_
#define NODE_H_

/**
 * 
 */
template<typename ItemType>
class Node {
public:
	Node();
	Node(const ItemType& anItem);
	Node(const ItemType& anItem, Node<ItemType>* nextNodePtr);
	void setItem(const ItemType& anItem);
	void setNext(Node<ItemType>* nextNodePtr);
	ItemType getItem() const;
	Node<ItemType>* getNext() const;
	virtual ~Node();
private:
	ItemType item;
	Node<ItemType>* next;
};

#include "Node.cpp"

#endif /* NODE_H_ */
