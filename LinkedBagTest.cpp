/**
 * @file   ArrayBagTest.cpp
 * @author Jim Daehn
 * @brief  LinkedBag CPPUNIT test.
 */

#include "LinkedBagTest.h"
#include "LinkedBag.h"

CPPUNIT_TEST_SUITE_REGISTRATION(LinkedBagTest);

LinkedBagTest::LinkedBagTest() :
		bag(nullptr), anotherBag(nullptr), expectedBag(nullptr),
				actualBag(nullptr) {
}

LinkedBagTest::~LinkedBagTest() {
}

void LinkedBagTest::setUp() {
	bag = new LinkedBag<std::string>();
	bag->add("a");
	bag->add("b");
	bag->add("c");

	anotherBag = new LinkedBag<std::string>();
	anotherBag->add("b");
	anotherBag->add("b");
	anotherBag->add("d");
	anotherBag->add("e");

	expectedBag = new LinkedBag<std::string>();
}

void LinkedBagTest::tearDown() {
	delete bag;
	bag = nullptr;
	delete anotherBag;
	anotherBag = nullptr;
}

void LinkedBagTest::testUnionHasExpectedFrequencyOfA() {
	expectedBag->add("a");
	expectedBag->add("b");
	expectedBag->add("b");
	expectedBag->add("b");
	expectedBag->add("c");
	expectedBag->add("d");
	expectedBag->add("e");

	actualBag = bag->getUnionWithBag(anotherBag);
	CPPUNIT_ASSERT_EQUAL_MESSAGE(ASSERTION_MESSAGE,
			expectedBag->getFrequencyOf("a"), actualBag->getFrequencyOf("a"));
}

void LinkedBagTest::testUnionHasExpectedFrequencyOfB() {
	expectedBag->add("a");
	expectedBag->add("b");
	expectedBag->add("b");
	expectedBag->add("b");
	expectedBag->add("c");
	expectedBag->add("d");
	expectedBag->add("e");

	actualBag = bag->getUnionWithBag(anotherBag);
	CPPUNIT_ASSERT_EQUAL_MESSAGE(ASSERTION_MESSAGE,
			expectedBag->getFrequencyOf("b"), actualBag->getFrequencyOf("b"));
}

void LinkedBagTest::testUnionHasExpectedFrequencyOfC() {
	expectedBag->add("a");
	expectedBag->add("b");
	expectedBag->add("b");
	expectedBag->add("b");
	expectedBag->add("c");
	expectedBag->add("d");
	expectedBag->add("e");

	actualBag = bag->getUnionWithBag(anotherBag);
	CPPUNIT_ASSERT_EQUAL_MESSAGE(ASSERTION_MESSAGE,
			expectedBag->getFrequencyOf("c"), actualBag->getFrequencyOf("c"));
}

void LinkedBagTest::testUnionHasExpectedFrequencyOfD() {
	expectedBag->add("a");
	expectedBag->add("b");
	expectedBag->add("b");
	expectedBag->add("b");
	expectedBag->add("c");
	expectedBag->add("d");
	expectedBag->add("e");

	actualBag = bag->getUnionWithBag(anotherBag);
	CPPUNIT_ASSERT_EQUAL_MESSAGE(ASSERTION_MESSAGE,
			expectedBag->getFrequencyOf("d"), actualBag->getFrequencyOf("d"));
}

void LinkedBagTest::testUnionHasExpectedFrequencyOfE() {
	expectedBag->add("a");
	expectedBag->add("b");
	expectedBag->add("b");
	expectedBag->add("b");
	expectedBag->add("c");
	expectedBag->add("d");
	expectedBag->add("e");

	actualBag = bag->getUnionWithBag(anotherBag);
	CPPUNIT_ASSERT_EQUAL_MESSAGE(ASSERTION_MESSAGE,
			expectedBag->getFrequencyOf("e"), actualBag->getFrequencyOf("e"));
}

void LinkedBagTest::testIntersectionHasExpectedFrequencyOfA() {
	expectedBag->add("b");

	actualBag = bag->getIntersectionWithBag(anotherBag);
	CPPUNIT_ASSERT_EQUAL_MESSAGE(ASSERTION_MESSAGE,
			expectedBag->getFrequencyOf("a"), actualBag->getFrequencyOf("a"));
}

void LinkedBagTest::testIntersectionHasExpectedFrequencyOfB() {
	expectedBag->add("b");

	actualBag = bag->getIntersectionWithBag(anotherBag);
	CPPUNIT_ASSERT_EQUAL_MESSAGE(ASSERTION_MESSAGE,
			expectedBag->getFrequencyOf("b"), actualBag->getFrequencyOf("b"));
}

void LinkedBagTest::testIntersectionHasExpectedFrequencyOfC() {
	expectedBag->add("b");

	actualBag = bag->getIntersectionWithBag(anotherBag);
	CPPUNIT_ASSERT_EQUAL_MESSAGE(ASSERTION_MESSAGE,
			expectedBag->getFrequencyOf("c"), actualBag->getFrequencyOf("c"));
}

void LinkedBagTest::testIntersectionHasExpectedFrequencyOfD() {
	expectedBag->add("b");

	actualBag = bag->getIntersectionWithBag(anotherBag);
	CPPUNIT_ASSERT_EQUAL_MESSAGE(ASSERTION_MESSAGE,
			expectedBag->getFrequencyOf("d"), actualBag->getFrequencyOf("d"));
}

void LinkedBagTest::testIntersectionHasExpectedFrequencyOfE() {
	expectedBag->add("b");

	actualBag = bag->getIntersectionWithBag(anotherBag);
	CPPUNIT_ASSERT_EQUAL_MESSAGE(ASSERTION_MESSAGE,
			expectedBag->getFrequencyOf("e"), actualBag->getFrequencyOf("e"));
}

void LinkedBagTest::testDifferenceHasExpectedFrequencyOfA() {
	expectedBag->add("a");
	expectedBag->add("c");

	actualBag = bag->getDifferenceWithBag(anotherBag);
	CPPUNIT_ASSERT_EQUAL_MESSAGE(ASSERTION_MESSAGE,
			expectedBag->getFrequencyOf("a"), actualBag->getFrequencyOf("a"));
}

void LinkedBagTest::testDifferenceHasExpectedFrequencyOfB() {
	expectedBag->add("a");
	expectedBag->add("c");

	actualBag = bag->getDifferenceWithBag(anotherBag);
	CPPUNIT_ASSERT_EQUAL_MESSAGE(ASSERTION_MESSAGE,
			expectedBag->getFrequencyOf("b"), actualBag->getFrequencyOf("b"));
}

void LinkedBagTest::testDifferenceHasExpectedFrequencyOfC() {
	expectedBag->add("a");
	expectedBag->add("c");

	actualBag = bag->getDifferenceWithBag(anotherBag);
	CPPUNIT_ASSERT_EQUAL_MESSAGE(ASSERTION_MESSAGE,
			expectedBag->getFrequencyOf("c"), actualBag->getFrequencyOf("c"));
}

void LinkedBagTest::testDifferenceHasExpectedFrequencyOfD() {
	expectedBag->add("a");
	expectedBag->add("c");

	actualBag = bag->getDifferenceWithBag(anotherBag);
	CPPUNIT_ASSERT_EQUAL_MESSAGE(ASSERTION_MESSAGE,
			expectedBag->getFrequencyOf("d"), actualBag->getFrequencyOf("d"));
}

void LinkedBagTest::testDifferenceHasExpectedFrequencyOfE() {
	expectedBag->add("a");
	expectedBag->add("c");

	actualBag = bag->getDifferenceWithBag(anotherBag);
	CPPUNIT_ASSERT_EQUAL_MESSAGE(ASSERTION_MESSAGE,
			expectedBag->getFrequencyOf("e"), actualBag->getFrequencyOf("e"));
}
